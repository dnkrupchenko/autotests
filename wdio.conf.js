require('dotenv').config();
const path = require('path');
const os = require('os');
const chai = require('chai');
const chaiWebdriver = require('chai-webdriverio').default;
const requireGlob = require('require-glob');
const getRandom = require('./get-random.js');

const config = {
  isDevelopment: process.env.NODE_ENV !== 'production',
  // URL тестируемого стенда
  baseUrl: process.env.URL || (() => { throw new Error('Не задан целевой хост для тестирования'); })(),
};

config.maxInstances = process.env.CONCURRENCY || config.isDevelopment ? 1 : os.cpus().length;

config.services = config.isDevelopment ? ['selenium-standalone'] : [];

const logLevels = ['silent', 'verbose', 'command', 'data', 'result', 'error'];
if (process.env.LOG_LEVEL && logLevels.includes(process.env.LOG_LEVEL)) {
  config.logLevel = process.env.LOG_LEVEL;
} else {
  config.logLevel = logLevels[config.isDevelopment ? 1 : 0];
}

// Для простоты понимания назовём это списком браузеров, на которых надо запускать тесты
const browsersListDevelopment = [{ browserName: 'chrome' }];
const browsersListProduction = [
  {
    browserName: 'chrome',
  },
  // Выключил, потому что в firefox не работают селекторы по id и усё ломается
  // {
  //   browserName: 'firefox',
  //   marionette: true,
  // },
  // Пока выключим, потом надо будет через аргументы пробросить список браузеров
  // {
  //   browserName: 'safari',
  // },
];
config.capabilities = config.isDevelopment ? browsersListDevelopment : browsersListProduction;

if (process.env.SELENIUM_HUB_HOST) {
  config.host = process.env.SELENIUM_HUB_HOST;
}

if (process.env.SELENIUM_HUB_PORT) {
  config.port = process.env.SELENIUM_HUB_PORT;
}

exports.config = {
  ...config,
  //
  // ====================
  // Runner Configuration
  // ====================
  //
  // WebdriverIO allows it to run your tests in arbitrary locations (e.g. locally or
  // on a remote machine).
  runner: 'local',

  // ==================
  // Specify Test Files
  // ==================
  // Define which test specs should run. The pattern is relative to the directory
  // from which `WebdriverIO` was called. Notice that, if you are calling `wdio` from an
  // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
  // directory is where your package.json resides, so `wdio` will be called from there.
  //
  specs: [
    './specs/**/*.js'
  ],
  // Patterns to exclude.
  exclude: [
    // './test/specs/file-to-exclude.js'
  ],
  // аргументы запуска selenium
  seleniumArgs: {
    javaArgs: [
      `-Dwebdriver.edge.driver=${path.join(__dirname, './drivers/MicrosoftWebDriver.exe')}`,
    ],
  },

  //
  // ============
  // Capabilities
  // ============
  // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
  // time. Depending on the number of capabilities, WebdriverIO launches several test
  // sessions. Within your capabilities you can overwrite the spec and exclude options in
  // order to group specific specs to a specific capability.
  //
  // First, you can define how many instances should be started at the same time. Let's
  // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
  // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
  // files and you set maxInstances to 10, all spec files will get tested at the same time
  // and 30 processes will get spawned. The property handles how many capabilities
  // from the same test should run tests.
  //
  maxInstances: 15,

  capabilities: [
    {
      // maxInstances can get overwritten per capability. So if you have an in-house Selenium
      // grid with only 5 Chrome instances available you can make sure that not more than
      // 5 instances get started at a time.
      maxInstances: 1,
      browserName: 'chrome',
      'goog:chromeOptions': {
        // to run chrome headless the following flags are required
        // (see https://developers.google.com/web/updates/2017/04/headless-chrome)
        // args: ['--headless', '--disable-gpu'],
      }
    },
  ],
  //
  // ===================
  // Test Configurations
  // ===================
  // Define all options that are relevant for the WebdriverIO instance here
  //
  // By default WebdriverIO commands are executed in a synchronous way using
  // the wdio-sync package. If you still want to run your tests in an async way
  // e.g. using promises you can set the sync option to false.
  sync: true,
  logLevel: 'silent', // Level of logging verbosity: silent | verbose | command | data | result | error
  // Warns when a deprecated command is used
  deprecationWarnings: true,
  //
  // If you only want to run your tests until a specific amount of tests have failed use
  // bail (default is 0 - don't bail, run all tests).
  bail: 0,
  //
  // Папка для скриншотов проваленных кейсов
  // screenshotPath: './reports/fail-screenshots',
  // Таймаут по умолчанию для всех waitFor* команд.
  waitforTimeout: 30000, // ms
  // Таймаут запроса к Selenium server
  connectionRetryTimeout: 90000,
  // Количество попыток переотправки запроса к Selenium server
  connectionRetryCount: 3,

  framework: 'mocha',

  // Опции запуска mocha
  mochaOpts: {
    ui: 'bdd',
    // timeout: 90000,
    compilers: ['js:@babel/register'],
    timeout: config.isDevelopment ? 10e6 : 100000,
  },

  services: ['selenium-standalone'],

  reporters: ['spec', ['allure', {
          outputDir: 'allure-results',
          disableWebdriverStepsReporting: true,
          disableWebdriverScreenshotsReporting: true,
      }]],

  //
  // =====
  // Hooks
  // =====
  // WedriverIO provides several hooks you can use to interfere with the test process in order to enhance
  // it and to build services around it. You can either apply a single function or an array of
  // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
  // resolved to continue.
  //
  // Gets executed before test execution begins. At this point you can access all global
  // variables, such as `browser`. It is the perfect place to define custom commands.
  /**
     * Gets executed once before all workers get launched.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
  onPrepare(config, capabilities) {
    console.log('**** let\'s go ****');
  },
  /**
     * Gets executed just before initialising the webdriver session and test framework. It allows you
     * to manipulate configurations depending on the capability or spec.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
  beforeSession: function (config, capabilities, specs) {
    require('@babel/register');
  },
  /**
    // Gets executed before test execution begins. At this point you can access all global
    // variables, such as `browser`. It is the perfect place to define custom commands.
    * @param {Array.<Object>} capabilities list of capabilities details
    * @param {Array.<String>} specs List of spec file paths that are to be run
    */
  before: function (capabilities, specs) {
    /**
       * Setup the Chai assertion framework
       */
    const chai    = require('chai');
    global.expect = chai.expect;
    global.assert = chai.assert;
    global.should = chai.should();
  },
  //


  before() {
    chai.config.includeStack = true;
    global.expect = chai.expect;
    chai.use(chaiWebdriver(browser, { defaultWait: 5000 }));
  },


  beforeSuite() {
    // эта дичь позволяет в repl'е перезагружать все хелперы без перезапуска wdio
    browser.reloadHelpers = function reloadHelpers(bustCache = true) {
      const helpersOriginal = requireGlob.sync(['./helpers/**/*.js', '!./helpers/utils/*'], { bustCache });
      browser.helpers = {};
      (function fn(helpers) {
        Object.entries(helpers)
          .forEach(([, helper]) => {
            if (typeof helper === 'object') {
              if (helper.__esModule !== true) { // eslint-disable-line no-underscore-dangle
                fn(helper);
              } else if (typeof helper.default === 'function') {
                if (!helper.default.name) {
                  throw new Error('Helper function must have name');
                }
                console.log(helper.default.name);
                browser.helpers[helper.default.name] = helper.default;
              }
            }
          });
      }(helpersOriginal));
    };

    // подключаем хелперы
    if (!browser.helpers) {
      browser.reloadHelpers(false);
    }

    browser.getRandom = getRandom;
  },

  afterTest({ err }) {
    if (err && process.env.DEBUG_ON_FAIL && browser) {
      browser.debug();
    }
  },

  /**
     * Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
  // Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
  // beforeTest: function (test) {
  // },
  /**
     * Runs before a WebdriverIO command gets executed.
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     */
  // beforeCommand: function (commandName, args) {
  // },
  /**
     * Runs after a WebdriverIO command gets executed
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     * @param {Number} result 0 - command success, 1 - command error
     * @param {Object} error error object if any
     */
  // afterCommand: function (commandName, args, result, error) {
  // },
  /**
     * Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
     * @param {Object} test test details
     */
  afterTest(test) {
    // console.log(test);
  },
  /**
     * Hook that gets executed after the suite has ended
     * @param {Object} suite suite details
     */
  // afterSuite: function (suite) {
  // },
  /**
     * Gets executed after all tests are done. You still have access to all global variables from
     * the test.
     * @param {Number} result 0 - test pass, 1 - test fail
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
  // after: function (result, capabilities, specs) {
  // },
  /**
     * Gets executed right after terminating the webdriver session.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
  // afterSession: function (config, capabilities, specs) {
  // },
  /**
     * Gets executed after all workers got shut down and the process is about to exit. It is not
     * possible to defer the end of the process using a promise.
     * @param {Object} exitCode 0 - success, 1 - fail
     */
  onComplete: function(exitCode) {
    console.log('**** that\'s it ****');
    }
};
