/**
 * @description Стандартный заказ с расчетом стоимости доставки
 * Тест написан на WebdriverIO + Mocha + Chai...
 * Видео того, как работает - http://www.screencast.com/t/TCJAr2DK
 * @type {Testcase}
 * @member Main features
 * @memberof Spec
*/
describe('Стандартный заказ товара', () => {
  it('Открываем сайт', () => {
    // открываем сайт
    browser.url('/');
    // пусть окно браузера открывается на весь экран...
    browser.maximizeWindow();
    // закрываем попап с выбором города (перекрывает "Каталог товаров")
    // TODO: вынести в хэлпер
    function closePopup() {
      const popup = $('.b-location-message');
      if (popup.isDisplayed()) {
        browser.execute(() => {
          const elem = document.querySelector('.b-location-message');
          elem.style.display = 'none';
        });
      }
    }
    closePopup();
  });

  it('Выбираем товар', () => {
    // Нажимаем на "Каталог товаров"
    $('.menu-1 a').click();
    // Смотрим, что блок с товарами появился на странице, фильтры есть
    $('.b-result-area').isDisplayed();
    $('.b-filters').isExisting();
    // Выбираем первый пункт "Продукт", кликаем на него
    $('.b-filters .w-filters:nth-child(2)').click();
    $('#filter-type_of_material li:nth-child(5)').moveTo();
  });

  it('Добавляем товар в корзину', () => {
    // Выбираем 2й элемент списка "Продукт"
    $('#filter-type_of_material li:nth-child(2)').click();
    $('.b-list-criteria').isDisplayed();
    // Добавляем первый товар в корзину
    $('.view-content .b-product:nth-child(1) .btn').click();
    $('.bottom-block').isExisting();
  });

  it('Выбираем кол-во товара', () => {
    // выбираем кол-во 2
    $('.info h4').moveTo();
    const spin = $('.ui-spinner-up');
    spin.click();
    spin.click();
  });

  it('Доставка товара', () => {
    // Переходим к доставке, выбираем "Доставка Saint-Gobain"
    $('.to-cart').click();
    $('.b-total').moveTo();
    // если "Доставка Saint-Gobain" не выбрана по умолчанию, то выбираем ее
    if (!$('#edit-delivery-type .form-item:nth-child(1) fake-radio-checked').isDisplayed()) {
      $('#edit-delivery-type .form-item:nth-child(1)').click();
    }
    // Нажимаем "Расчитать доставку/оформить заказ"
    $('.bot .checkout').click();
    browser.waitUntil(() => $('.b-form-inner').isDisplayed);
  });

  it('Вводим адрес доставки', () => {
    const city = 'Россия, Московская область, Подольск';
    $('#edit-customer-profile-shipping-string-address').setValue(city);
    $('#edit-customer-profile-shipping-delivery-submit-1').click();
    // идем к форме с инфой о ценах
    $('.count').moveTo();
    // ждем, пока появится инфа о цене доставки
    browser.waitUntil(() => $('.view-content .delivery:nth-of-type(3)').isExisting);
    // кликаем 'Продолжить'
    const checkout = $('.checkout-continue');
    checkout.moveTo();
    checkout.click();
    // проверяем, что дальше идет форма "Информация о покупателе"
    const label = $('.item-info-recipient .label');
    browser.waitUntil(() => label.getText() === 'ИНФОРМАЦИЯ О ПОКУПАТЕЛЕ');
    expect(label.getText()).to.equal('ИНФОРМАЦИЯ О ПОКУПАТЕЛЕ');
    // и т.д.
    // TODO: Дописать, добавить проверок расчета цен...
  });
});
